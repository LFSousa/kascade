const fs = require('fs')
    , util = require('util')
    , path = require('path');

function Kascade(relative = "") {

    this.cwd = path.join(process.cwd(), relative);
    this.files = [];
    this.latestAction = "include";
    this.latestPath = this.cwd;
    this.log = {
        debug: (...args) => {
            console.debug('\x1b[36m%s\x1b[0m', "[KASCADE]", ...args);
        },
        error: (...args) => {
            console.error('\x1b[41m%s\x1b[0m', "[KASCADE]", ...args);
        },
        info: (...args) => {
            console.info('\x1b[32m%s\x1b[0m', "[KASCADE]", ...args)
        }
    }
}

Kascade.prototype._locate = function(folderPath) {
    
}

Kascade.prototype.include = function(folderPath) {

    if (this.latestAction == "include") {
        this.latestPath = path.join(this.latestPath, folderPath);
    } else {
        this.latestPath = path.join(this.cwd, folderPath);
    }
    this.log.debug(`Including ${this.latestPath}`);
    this.latestAction = "include";
    return this;
}

Kascade.prototype.includeFile = function(filePath, _path) {

    _path = _path || this.latestPath;
    filePath = path.join(_path, filePath);
    this.log.debug("Including file", filePath);
    this.files.push(filePath);
    this.latestAction = "includeFile";
    return this;
}

Kascade.prototype.then = function(folderPath) {
    
    let _path = path.join(this.latestPath,folderPath);
    this.log.debug("Locating files at", _path);

    let files = fs.readdirSync(_path);
    files.filter(el => /\.js$/.test(el)).forEach(file => { this.includeFile(file, _path) });
    this.latestAction = "then";
    return this;
}

Kascade.prototype.into = function(end) {

    let tree = end;
    let error = false;
    this.files.forEach(file => {
        
        let branch = tree;
        file.replace(this.cwd, "")
        .split("/")
        .forEach(limb => {
            if (limb.length == 0) return;
            
            if (!limb.includes(".js")) {
                
                if (!branch[limb]) branch[limb] = {};

                branch = branch[limb];
            } else {

                try {
                    branch[limb.replace(".js", "")] = require(file)(end);
                } catch(e) {
                    this.log.error(`The required file ${file} could not been loaded!`);
                    this.log.error(e);
                    error = true;
                }
            }
        });
    })

    if (error) this.log.error("Work finished with errors!");
    else this.log.info(`Work finished!`);
}

module.exports = function(...args) {
    return new Kascade(...args);
};